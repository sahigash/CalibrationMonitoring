#!/bin/sh
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi
source ./setup.sh

./cal1Dplot config/config_thr.xml plots/66333_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066333.root
./cal1Dplot config/config_thr.xml plots/66337_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066337.root
./cal1Dplot config/config_thr.xml plots/66391_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066391.root
./cal1Dplot config/config_thr.xml plots/66427_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066427.root
./cal1Dplot config/config_thr.xml plots/66437_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066437.root
./cal1Dplot config/config_thr.xml plots/66477_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066477.root
./cal1Dplot config/config_thr.xml plots/66537_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066537.root
./cal1Dplot config/config_thr.xml plots/66567_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066567.root
./cal1Dplot config/config_thr.xml plots/66571_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066571.root
./cal1Dplot config/config_thr.xml plots/66583_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066583.root
./cal1Dplot config/config_thr.xml plots/66587_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066587.root
./cal1Dplot config/config_thr.xml plots/66802_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066802.root
./cal1Dplot config/config_thr.xml plots/66806_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066806.root
./cal1Dplot config/config_thr.xml plots/66927_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066927.root
./cal1Dplot config/config_thr.xml plots/66962_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066962.root
./cal1Dplot config/config_thr.xml plots/66971_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000066971.root
./cal1Dplot config/config_thr.xml plots/67054_thr.root /eos/atlas/user/s/sahigash/scanData/SCAN_S000067054.root
