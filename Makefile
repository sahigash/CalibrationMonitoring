TARGET		= historyplot cal1Dplot makeOfficialPlot

EXCLUDES	= ./src/historyplot.o ./src/cal1Dplot.o ./src/makeOfficialPlot.o
INC			= -I./
SRC			= ./src
SRCS		= $(wildcard $(SRC)/*.C)
OBJS		:= $(addsuffix .o, $(basename $(SRCS)))
OBJS		:= $(filter-out $(wildcard $(EXCLUDES)), $(OBJS))

ROOTCFLAGS	:= $(shell root-config --cflags)
ROOTLDFLAGS	:= $(shell root-config --ldflags)
ROOTLIBS	:= $(shell root-config --libs)
ROOTGLIBS	:= $(shell root-config --glibs)

LDFLAGS		= -O $(ROOTLDFLAGS)
CXXFLAGS	+= -std=c++11 -g -Wall -fPIC $(ROOTCFLAGS)
LIBS		+= $(ROOTLIBS) $(ROOTGLIBS) -lXMLIO 

# EXES = $(basename $(CAL_SRCS))

# .SUFFIXES:
# .SUFFIXES: .o .C
.C.o:
	@echo "Start Compiling"
	g++ $(CXXFLAGS) ${INC} -c $< -o ${patsubst %.C,%.o,$<}

all:  $(TARGET)

cal1Dplot: ${OBJS} ./src/cal1Dplot.o
	@echo "Now Make cal1Dplot:"
	@echo ${OBJS}	
	g++ $(CXXFLAGS) $(LDFLAGS) ${INC} -o $@ ${OBJS} ./src/cal1Dplot.o $(LIBS)

historyplot: ${OBJS} ./src/historyplot.o
	@echo "Now Make historyplot:"
	@echo ${OBJS}	
	g++ $(CXXFLAGS) $(LDFLAGS) ${INC} -o $@ ${OBJS} ./src/historyplot.o $(LIBS)

makeOfficialPlot: ${OBJS} ./src/makeOfficialPlot.o
	@echo "Now Make monitoring plot:"
	@echo ${OBJS}	
	g++ $(CXXFLAGS) $(LDFLAGS) ${INC} -o $@ ${OBJS} ./src/makeOfficialPlot.o $(LIBS)

clean:
	rm -f $(TARGET) ${OBJS} ./src/cal1Dplot.o ./src/historyplot.o ./src/makeOfficialPlot.o
