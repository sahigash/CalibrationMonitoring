#ifndef PIX_HISTORY_SCAN_BASE_H
#define PIX_HISTORY_SCAN_BASE_H

#include "include/util.h"
#include "include/plotutil.h"
#include "include/pixhistoryplotparam.h"

class ScanBase
{
private:

protected:
    PixFileHistPair*         m_pFilePair;
    PixHistoryPlotParameter* m_pCfgParam;

    virtual bool LoadFile              ( const PixStructType&    structType,
                                         TDirectory*             pDir );

    virtual bool FillHistsForPix       ( const String&           structTypeStr,
                                         const int&              col,
                                         const int&              row,
                                         const double&           val );

    virtual bool FillHistsForIBL       ( const String&           structTypeStr,
                                         const int&              col,
                                         const int&              row,
                                         const double&           val );

    virtual bool LoadModuleForPixel    ( TDirectory*             pDir,
                                         const String&           slotFolder,
                                         PixStructType           structType ) = 0;
    virtual bool LoadModuleForIBL      ( TDirectory*             pDir,
                                         const String&           slotFolder,
                                         PixStructType           structType ) = 0;

    virtual bool LoadHist              ( TDirectory*             pDir,
                                         const PixStructType&    structType,
                                         const String&           totMode,
                                         const String&           mapMode ) = 0;


public:
    ScanBase             ( PixHistoryPlotParameter* pCfgParam );
    virtual ~ScanBase    ( );

    virtual bool CreatePlot     ( );
    void         SetFilePair    ( PixFileHistPair* pFilePair ) { m_pFilePair = pFilePair; }
};

#endif // PIX_HISTORY_SCAN_BASE_H
