#ifndef PIX_HISTORY_CALMON_HISTORYPLOT_H
#define PIX_HISTORY_CALMON_HISTORYPLOT_H

#include "include/calMonBase.h"

class CalMonHistoryPlot : public CalibrationMonitoringBase
{
private:
    std::list< PixFileHistPair* > m_histList;
    void DeleteListComponents( );
    bool CreatePlots     ( );
    bool PrepareHists    ( );
    bool CreateTHist     ( const String&                  fileNameStr,
                           std::map< String, TH1F* >*     pMap );
    bool DrawOrSavePlots         ( );

    String   GetXAxisLabel       ( );
    String   GetYAxisLabel       ( );

    double   GetXAxisValFromFileName( const String& fileName );

    void     AddPlot( std::map< String, TGraph* >* pPlotFileTable,
                      const String&                name );

    void SetYRange( TH1* pHist );
    void ReLabelXAxis( TH1* pPlot );

public:
    CalMonHistoryPlot( const String& FileName );
    ~CalMonHistoryPlot( );

    bool Execute         ( );
    bool Initialize      ( );
    bool Finalize        ( );
    
};

#endif // PIX_HISTORY_CALMON_HISTORYPLOT_H

