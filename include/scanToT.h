#ifndef PIX_HISTORY_SCAN_TOT_H
#define PIX_HISTORY_SCAN_TOT_H

#include "include/scanBase.h"

class ScanToT : public ScanBase
{
public:
    ScanToT             ( PixHistoryPlotParameter* pCfgParam );
    virtual ~ScanToT    ( );

private:
    virtual bool LoadHist              ( TDirectory*             pDir,
                                         const PixStructType&    structType,
                                         const String&           totMode,
                                         const String&           mapMode );

    virtual bool LoadModuleForPixel    ( TDirectory*             pDir,
                                         const String&           slotFolder,
                                         PixStructType           structType );
    virtual bool LoadModuleForIBL      ( TDirectory*             pDir,
                                         const String&           slotFolder,
                                         PixStructType           structType );
};

#endif // PIX_HISTORY_SCAN_TOT_H

