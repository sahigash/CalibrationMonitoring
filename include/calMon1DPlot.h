#ifndef PIX_HISTORY_CALMON_1DPLOT_H
#define PIX_HISTORY_CALMON_1DPLOT_H

#include "include/calMonBase.h"

class CalMon1DPlot : public CalibrationMonitoringBase
{
private:
    std::list< PixFileHistPair* > m_histList;
    void DeleteListComponents( );
    bool CreatePlots     ( );
    bool PrepareHists    ( );
    bool CreateTHist     ( const String&                  fileNameStr,
                           std::map< String, TH1F* >*     pMap );
    bool DrawOrSavePlots         ( );

    String   GetXAxisLabel       ( );
    String   GetYAxisLabel       ( );

public:
    CalMon1DPlot( const String& FileName );
    ~CalMon1DPlot( );

    bool Execute         ( );
    bool Initialize      ( );
    bool Finalize        ( );

    bool ResetFileset    ( std::list< String >* pFileList );
    bool ResetOutputFile ( const String& output );
};

#endif // PIX_HISTORY_CALMON_1DPLOT_H

