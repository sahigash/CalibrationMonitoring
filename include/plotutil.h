#ifndef PIX_HISTORY_PLOT_UTIL_H
#define PIX_HISTORY_PLOT_UTIL_H

#include "TH1.h"
#include "TH2.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TFile.h"
#include "TKey.h"

#include "include/util.h"
#include "include/pixhistoryplotparam.h"

const String PIX_ROD_PREFIX                       = "ROD_";
const String PIX_BARREL_HDR                       = "B";
const String PIX_DISK_HDR                         = "D";
const String PIX_IBL_HDR                          = "I";
const String PIX_LAYER_HDR                        = "L";
const String PIX_BISTAVE_HDR                      = "B";
const String PIX_SLOT_HDR                         = "S";
const String PIX_STAVE_HDR                        = "S";
const String PIX_MODULE_HDR                       = "M";

const String PIX_BLAYER_STR                       = "B-Layer";
const String PIX_LAYER1_STR                       = "Layer-1";
const String PIX_LAYER2_STR                       = "Layer-2";
const String PIX_ECALL_STR                        = "EndCaps";
const String PIX_ECA1_STR                         = "ECA1";
const String PIX_ECA2_STR                         = "ECA2";
const String PIX_ECA3_STR                         = "ECA3";
const String PIX_ECC1_STR                         = "ECC1";
const String PIX_ECC2_STR                         = "ECC2";
const String PIX_ECC3_STR                         = "ECC3";
const String PIX_IBL_STR                          = "IBL";

const String PIX_BLAYER_HDR_STR                   = "L0";
const String PIX_LAYER1_HDR_STR                   = "L1";
const String PIX_LAYER2_HDR_STR                   = "L2";
const String PIX_IBL_HDR_STR                      = "LI";
const String PIX_ECA1_HDR_STR                     = "D1A";
const String PIX_ECA2_HDR_STR                     = "D2A";
const String PIX_ECA3_HDR_STR                     = "D3A";
const String PIX_ECC1_HDR_STR                     = "D1C";
const String PIX_ECC2_HDR_STR                     = "D2C";
const String PIX_ECC3_HDR_STR                     = "D3C";
 
const String PIX_HISTORY_HIST_PIXALL              = "Pixel";
const String PIX_HISTORY_HIST_PIXL0               = "Pixel_B-Layer";
const String PIX_HISTORY_HIST_PIXL1               = "Pixel_Layer-1";
const String PIX_HISTORY_HIST_PIXL2               = "Pixel_Layer-2";
const String PIX_HISTORY_HIST_PIXEC               = "Pixel_ECBOTH";
const String PIX_HISTORY_HIST_PIXECA              = "Pixel_ECA";
const String PIX_HISTORY_HIST_PIXECC              = "Pixel_ECC";
const String PIX_HISTORY_HIST_IBLALL              = "IBL";
const String PIX_HISTORY_HIST_IBLPLANAR           = "IBL_Planar";
const String PIX_HISTORY_HIST_IBL3D               = "IBL_3D";
const String PIX_HISTORY_HIST_IBL3DFBK            = "IBL_3D_FBK";
const String PIX_HISTORY_HIST_IBL3DCNM            = "IBL_3D_CNM";

const String PIX_HISTORY_XAXIS_FILENAME           = "Scan File Name";
const String PIX_HISTORY_XAXIS_DATE               = "Date";
const String PIX_HISTORY_XAXIS_LUMINOSITY         = "Integrated Luminosity [fb^{-1}]";
const String PIX_HISTORY_XAXIS_TID                = "TID [Mrad]";

const String PIX_HISTORY_YAXIS_THRESHOLD          = "Threshold [e^{-}]";
const String PIX_HISTORY_YAXIS_ENC                = "ENC [e^{-}]";
const String PIX_HISTORY_YAXIS_CHI2               = "Chi2";
const String PIX_HISTORY_YAXIS_TOT                = "ToT [B.C.]";
const String PIX_HISTORY_YAXIS_T0                 = "T0";
const String PIX_HISTORY_YAXIS_MEAN               = "Mean";
const String PIX_HISTORY_YAXIS_RMS                = "RMS of Mean";

const String PIX_HISTORY_PIXTYPE_NORMAL           = "Normal";
const String PIX_HISTORY_PIXTYPE_GANGED           = "Ganged";
const String PIX_HISTORY_PIXTYPE_INTERGANGED      = "Inter_Ganged";
const String PIX_HISTORY_PIXTYPE_LONG             = "Long";
const String PIX_HISTORY_PIXTYPE_LONG_GANGED      = "Long_Ganged";
const String PIX_HISTORY_PIXTYPE_LONG_INTERGANGED = "Long_Inter_Ganged";

const String PIX_THR_SCURVE_MODE_MEAN   = "SCURVE_MEAN";
const String PIX_THR_SCURVE_MODE_SIGMA  = "SCURVE_SIGMA";
const String PIX_THR_SCURVE_MODE_CHI2   = "SCURVE_CHI2";
const String PIX_THR_MAP_MODE_MEAN      = "MeanMap";
const String PIX_THR_MAP_MODE_SIGMA     = "SigmaMap";
const String PIX_THR_MAP_MODE_CHI2      = "Chi2Map";

const String PIX_TOT_MEAN          = "TOT_MEAN";
const String PIX_TOT_MAP_MODE_MEAN = "MeanMap";

typedef std::pair< String, std::map< String, TH1F* >* > PixFileHistPair;

enum PixStructType {
    BLayer,
    Layer1,
    Layer2,
    ECALL,
    ECA1,
    ECA2,
    ECA3,
    ECC1,
    ECC2,
    ECC3,
    IBL,
    IBLPLANAR,
    IBL3D,
};

enum PixType {
    PIX_TYPE_UNKNOWN,
    PIX_TYPE_NORMAL,
    PIX_TYPE_GANGED,
    PIX_TYPE_INTERGANGED,
    PIX_TYPE_LONG,
    PIX_TYPE_LONG_GANGED,
    PIX_TYPE_LONG_INTERGANGED,
};

/////////////////////////////////////////////////////////////////////////
//
// CLASS : PixHistoryPlotParameter
//
/////////////////////////////////////////////////////////////////////////
class CalMonPlotUtil
{
public:
    static bool     IsPixel                ( PixHistoryPlotParameter* pCfgParam );
    static bool     IsPixel                ( const PixStructType&     type );
    static bool     IsIBL                  ( PixHistoryPlotParameter* pCfgParam );
    static bool     IsIBL                  ( const PixStructType&     type );

    static bool     IsBarrelLayers         ( const PixStructType&     structType );
    static bool     IsEndCaps              ( const PixStructType&     structType );

    static PixType  GetPixTypeForPixel     ( const int&               col,
                                             const int&               row );
    static PixType  GetPixTypeForIBL       ( const int&               col,
                                             const int&               row );

    static int      GetNumOfHists          ( const PixDisplayType&    dispType );

    static void     CreateScanTypeStr      ( PixHistoryPlotParameter* pCfgParam,
                                             std::list< String >*     pScanTypeStrList );

    static bool     CreateCrateNameStr     ( const PixStructType&     structType,
                                             std::list< String >&     crateNameList );

    static void     CreateStructTypeStr    ( const PixStructType&     structType,
                                             String&                  str );

    static void     CreateStructTypeHdrStr ( const PixStructType&     structType,
                                             std::list< String >&     strList );

    static String   CreateScanTypeStrForPix( const PixStructType&     structType );

    static String   CreatePixTypeStr       ( const PixType&           pixType );

    static void     GetNumberforDisks      ( int&                     num,
                                             const String&            crateNameStr );

    static bool     GetModuleInfo          ( const String&            feName,
                                             bool&                    isAside,
                                             int&                     moduleNo,
                                             int&                     staveNo );

    static bool     GetSensorTypeForIBL    ( const String&            feName,
                                             PixHistoryPlotParameter* pParam,
                                             std::list< String >&     strList );

    static bool     IsFBKSensor            ( const int&               staveNo,
                                             const bool&              isAside );

    static bool     GetPixTypeStr          ( PixHistoryPlotParameter* pParam,
                                             const int&               col,
                                             const int&               row,
                                             String&                  str );

    static String   GetScanTypeFromKey     ( PixHistoryPlotParameter* pCfgParam,
                                             const String&            key );


    static bool     GetTObjListFromTDir    ( TDirectory*              pDir,
                                             std::list< TObject* >*   pObjList,
                                             const bool&              clone = true );

    static void     Get1DHistBinning       ( PixHistoryPlotParameter* pCfgParam,
                                             int&                     bin,
                                             double&                  min,
                                             double&                  max );

};

#endif // PIX_HISTORY_PLOT_UTIL_H
