#ifndef SH_DEF_H
#define SH_DEF_H

// ---------------------------------------------------------------
// Include standard C++ header files
// ---------------------------------------------------------------

// sys
#include <sys/types.h>
#include <sys/stat.h>

// C++ basics
#include <iostream>
#include <cstring>
#include <sstream>

#include <cmath>
#include <ctime>
#include <cstdio>

// STL
#include <set>
#include <vector>
#include <map>
#include <list>

// typedef std namespace data types
typedef std::string       String;
typedef std::stringstream StringStream;

// for debug
#define DEBUG(val) std::cout<<"Debugging : "<<val<<std::endl;

#endif // SH_DEF_H
