//////////////////////////////////////////////////////////////////
//
// Utility
//
//////////////////////////////////////////////////////////////////
#ifndef SH_UTIL_H
#define SH_UTIL_H

// COMMON
#include "include/def.h"

// File operation
#include <dirent.h>

class ShUtil
{
public:

    //////////////////////////////////////////////////////////////////
    //
    // Converter
    //
    //////////////////////////////////////////////////////////////////

    // ---------------------------------------------------------------
    // -> string data converter
    template <typename T> static String ToString( const T& val )
    {
        String str = "";
        StringStream stream;

        stream << val;
        stream >> str;

        return str;
    }

    // ---------------------------------------------------------------
    // -> string data converter
    static int StrToInt( const String& str )
    {
        StringStream stream;
        int retVal = 0;
        
        stream << str;
        stream >> retVal;

        return retVal;
    }

        // ---------------------------------------------------------------
    // -> string data converter
    static double StrToDouble( const String& str )
    {
        StringStream stream;
        double retVal = 0.0;
        
        stream << str;
        stream >> retVal;

        return retVal;
    }

    // ---------------------------------------------------------------
    // Convert timee unit
    // ---------------------------------------------------------------
    // ---------------------------------------------------------------
    // hour -> minute
    static double HourToMin( double hour )
    {
        return hour * 60.0;
    }

    // ---------------------------------------------------------------
    // hour -> second
    static double HourToSec( double hour )
    {
        return hour * 3600.0;
    }

    // ---------------------------------------------------------------
    // minute -> hour
    static double MinToHour( double min )
    {
        return min / 60.0;
    }

    // ---------------------------------------------------------------
    // minute -> second
    static double MinToSec( double min )
    {
        return min * 60.0;
    }

    // ---------------------------------------------------------------
    // second -> hour
    static double SecToHour( double sec )
    {
        return sec * 3600.0;
    }

    // ---------------------------------------------------------------
    // second -> minute
    static double SecToMin( double sec )
    {
        return sec * 60.0;
    }

    // ---------------------------------------------------------------
    // Console Output
    // ---------------------------------------------------------------
    // ---------------------------------------------------------------
    // only applied for string value.
    // eg. MuUitl::Cout( "Example" );
    template <typename T> static void Cout( const T& val )
    {
        std::cout << val << std::endl;
    }

    // ---------------------------------------------------------------
    // output error message
    template <typename T> static void Cerr( const T& val )
    {
        std::cout << "ShUtil Error: " << val << std::endl;
    }

    // ---------------------------------------------------------------
    // output error message
    template <typename T> static void Cwarn( const T& val )
    {
        std::cout << "ShUtil Warning: " << val << std::endl;
    }

    // ---------------------------------------------------------------
    // output error message
    template <typename T> static void Cinfo( const T& val )
    {
        std::cout << "ShUtil Information: " << val << std::endl;
    }

    //////////////////////////////////////////////////////////////////
    //
    // Comparetor
    //
    //////////////////////////////////////////////////////////////////
    // ---------------------------------------------------------------
    // Comparing "double" value
    // at 10e-6 (the significance of float value)
    // return :
    //     1 for first > second
    //    -1 for first < second
    //     0 for first = second
    static int CompareDouble( const double& first, const double& second )
    {
        if( fabs( first - second ) < 0.000001 ) {
            return 0;
        }
        else {
            if( first > second ) {
                return 1;
            }
            else {
                return -1;
            }
        }
    }

    // ---------------------------------------------------------------
    // Comparing "double" value to Zero
    // at 10e-6 (the significance of float value)
    static bool IsZero( const double& val )
    {
        if( CompareDouble( val, 0.0 ) == 0 )
            return true;

        return false;
    }

    //////////////////////////////////////////////////////////////////
    //
    // File Checker
    //
    //////////////////////////////////////////////////////////////////
    // ---------------------------------------------------------------
    // return:
    //    true ...exist 
    //    false...NOT exist 
    static bool ExistFile( const String& filePath )
    {
        bool exist = false;
        FILE* fp = fopen( filePath.c_str( ), "r" );
        if( fp != nullptr ){
            exist = true;
            fclose( fp );
        }
        return exist;
    }

    // ---------------------------------------------------------------
    // Check directry
    // return:
    //    true ...exist 
    //    false...NOT exist 
    // comment:
    //    faster to use S_ISDIR( ) instead of opendir( )?
    static bool ExistDir( const String& dirPath )
    {
        bool exist = false;
        DIR* dp = opendir( dirPath.c_str( ) );
        if( dp != nullptr ) {
            exist = true;
            closedir( dp );
        }
        return exist;
    }

    // ---------------------------------------------------------------
    // Check directry: if the directory is not found, create it
    static void ExistCreateDir( const String& dirPath )
    {
        if( ExistDir( dirPath ) == true ) return;

        // create directory
        if( mkdir( dirPath.c_str( ), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ) == 0 ) {
            String output = "create directory : " + dirPath;
            Cinfo( output );
        }
        return;
    }

    // ---------------------------------------------------------------
    // Check directry belonging to file path
    // return:
    //    true ...exist 
    //    false...NOT exist 
    static bool ExistFilePathDir( const String& filePath )
    {
        int index = filePath.rfind( "/", filePath.size( ) - 1 );
        String dirPath = filePath.substr( 0, index );

        return ShUtil::ExistDir( dirPath );
    }

    // ---------------------------------------------------------------
    // Get file name from full path
    // return: file name
    static String GetFileName( const String& path )
    {
        size_t pos1 = path.rfind( '/' );
        if( pos1 != String::npos ) {
            return path.substr( pos1 + 1, path.size( ) - pos1 - 1 );
        }
        
        return path;
    }

    // ---------------------------------------------------------------
    // Extract extension from file name
    // return: file name (without extension)
    static String ExtractPathWithoutExt( const String& path )
    {
        String::size_type pos;
        if( ( pos = path.find_last_of( "." ) ) == String::npos ) {
            return path;
        }

        return path.substr( 0, pos );
    }

    // ---------------------------------------------------------------
    // get file path list from input directory
    // argument:
    //     [input]  inputDir      ... input directory (Don't include "/" at the end of directory name!!!)
    //     [output] pFilePathList ... relative file path from input directory
    //     [input]  recursive     ... if true, output file path recursively
    //     [input]  fileKey       ... picking up only containing "fileKey" phrase in a file name
    //     [input]  dirKey        ... picking up only containing "dirKey" phrase in a directory name
    // return: 
    //     true ... success
    //     false... failure
    static bool getFilePathList( const String&        inputDir,
                                 std::list< String >* pFilePathList,
                                 const bool&          recursive    = true,
                                 const String&        fileKey      = "",
                                 const String&        dirKey       = "" )
    {
        if( pFilePathList == nullptr ) return false;
        // if( ExistDir( inputDir ) == false ) return false;

        bool retVal = true;
        DIR* dp = opendir( inputDir.c_str( ) );
        if( dp != nullptr ) {
            struct dirent* entry;
            struct stat st;
            String name = "";
            
            while( (entry = readdir( dp ) ) != nullptr ) {
                name = entry->d_name;
                String filePath = inputDir + "/";
                filePath += name;
                
                // skip "." and ".."
                if( name.length( ) <= 0 || name == "." || name == ".." ) continue;

                // skip stat file
                if( stat( filePath.c_str( ), &st ) == -1 ) continue;

                if( S_ISDIR( st.st_mode ) == true ) {
                    if( recursive == true ) {
                        if( getFilePathList( filePath, pFilePathList, recursive, fileKey, dirKey ) == false )
                            retVal = false;
                    }
                }
                else {
                    if( fileKey.length( ) > 0 && name.find( fileKey ) == String::npos ) continue;
                    if( dirKey.length( ) > 0 && filePath.find( dirKey ) == String::npos ) continue;
                    pFilePathList->push_back( filePath );
                }
            }
        }
        closedir( dp );

        return retVal;
    }

    //////////////////////////////////////////////////////////////////
    //
    // Calculation
    //
    //////////////////////////////////////////////////////////////////
    // ---------------------------------------------------------------
    // Get Sum of Scuares
    // 
    static double GetSumOfSqrt( const double& val1, const double& val2 )
    {
        return sqrt( val1 * val1 + val2 * val2 );
    }
    static double GetSumOfSqrt( const double& val1, const double& val2, const double& val3 )
    {
        return sqrt( val1*val1 + val2*val2 + val3*val3 );
    }
};

#endif // SH_UTIL_H

