#ifndef PIX_HISTORY_CALMON_BASE_H
#define PIX_HISTORY_CALMON_BASE_H

#include "include/util.h"
#include "include/plotutil.h"
#include "include/pixhistoryplotparam.h"

class CalibrationMonitoringBase
{
private:
protected:

    PixHistoryPlotConfig* m_pPlotCfg;
    PixHistoryPlotParameter* m_pCfgParam;
    String m_cfgFileName;

    bool m_isInit;


    virtual String   GetXAxisLabel       ( ) = 0;;
    virtual String   GetYAxisLabel       ( ) = 0;;

public:

    CalibrationMonitoringBase( const String& cfgFileName );
    ~CalibrationMonitoringBase( );

    virtual bool Initialize              ( );
    virtual bool Finalize                ( );
    virtual bool Execute                 ( );

    virtual bool CreatePlots             ( ) = 0;
    virtual bool PrepareHists            ( ) = 0;
    virtual bool CreateTHist             ( const String&                  fileNameStr,
                                           std::map< String, TH1F* >*     pMap ) = 0;
    virtual bool DrawOrSavePlots         ( ) = 0;
};

#endif // PIX_HISTORY_CALMON_BASE_H
