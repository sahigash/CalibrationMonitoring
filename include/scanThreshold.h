#ifndef PIX_HISTORY_SCAN_THR_H
#define PIX_HISTORY_SCAN_THR_H

#include "include/scanBase.h"

class ScanThreshold : public ScanBase
{
public:
    ScanThreshold             ( PixHistoryPlotParameter* pCfgParam );
    virtual ~ScanThreshold    ( );

private:
    virtual bool LoadHist                ( TDirectory*             pDir,
                                           const PixStructType&    structType,
                                           const String&           scanMode,
                                           const String&           mapMode );

    virtual bool LoadModuleForPixel      ( TDirectory*             pDir,
                                           const String&           slotFolder,
                                           PixStructType           structType );
    virtual bool LoadModuleForIBL        ( TDirectory*             pDir,
                                           const String&           slotFolder,
                                           PixStructType           structType );

    virtual bool CreateSCurveParamStr    ( String&                 sCurveModeStr,
                                           String&                 mapModeStr );

};

#endif // PIX_HISTORY_SCAN_THR_H

