#ifndef PIX_HISTORY_PLOT_PARAM_H
#define PIX_HISTORY_PLOT_PARAM_H

#include "include/def.h"
#include "include/pixhistoryplotconfig.h"

// DEFINITION

// XML
const String PIX_HISTORY_XMLTAG_CONFIG            = "Configuration";

const String PIX_HISTORY_XMLTAG_READMODE          = "ReadMode";
const String PIX_HISTORY_XML_READMODE_SCAN        = "SCAN";
const String PIX_HISTORY_XML_READMODE_HIST        = "HIST";

const String PIX_HISTORY_XMLTAG_SCANFILE          = "ScanFile";

const String PIX_HISTORY_XMLTAG_SCANTYPE          = "ScanType";
const String PIX_HISTORY_XML_SCANTYPE_THRESHOLD   = "Threshold";
const String PIX_HISTORY_XML_SCANTYPE_ENC         = "ENC";
const String PIX_HISTORY_XML_SCANTYPE_CHI2        = "Chi2";
const String PIX_HISTORY_XML_SCANTYPE_TOT         = "ToT";
const String PIX_HISTORY_XML_SCANTYPE_T0          = "T0";
const String PIX_HISTORY_XML_SCANTYPE_MEAN        = "Mean";
const String PIX_HISTORY_XML_SCANTYPE_RMS         = "RMS";

const String PIX_HISTORY_XMLTAG_DISP              = "Display";
const String PIX_HISTORY_XML_DISP_PIXALL          = "Pixel_All";
const String PIX_HISTORY_XML_DISP_PIXL0           = "Pixel_Layer-0";
const String PIX_HISTORY_XML_DISP_PIXL1           = "Pixel_Layer-1";
const String PIX_HISTORY_XML_DISP_PIXL2           = "Pixel_Layer-2";
const String PIX_HISTORY_XML_DISP_PIXEC           = "Pixel_EC";
const String PIX_HISTORY_XML_DISP_PIXECA          = "Pixel_ECA";
const String PIX_HISTORY_XML_DISP_PIXECC          = "Pixel_ECC";
const String PIX_HISTORY_XML_DISP_IBLALL          = "IBL_All";
const String PIX_HISTORY_XML_DISP_IBLPLANAR       = "IBL_Planar";
const String PIX_HISTORY_XML_DISP_IBL3D           = "IBL_3D";
const String PIX_HISTORY_XML_DISP_IBL3DFBK        = "IBL_3D_FBK";
const String PIX_HISTORY_XML_DISP_IBL3DCNM        = "IBL_3D_CNM";

const String PIX_HISTORY_XMLTAG_SHOWALLPIXTYPE    = "ShowAllPixelType";

const String PIX_HISTORY_XMLTAG_XAXIS             = "HorizontalAxis";
const String PIX_HISTORY_XML_XAXIS_FILENAME       = "ScanFileName";
const String PIX_HISTORY_XML_XAXIS_DATE           = "Date";
const String PIX_HISTORY_XML_XAXIS_LUMINOSITY     = "Luminosity";
const String PIX_HISTORY_XML_XAXIS_TID            = "TID";
const String PIX_HISTORY_XML_XAXIS_CUSTOM         = "Custom";

const String PIX_HISTORY_XMLTAG_FILESAVEOPT       = "FileSaveOption";

const String PIX_HISTORY_XML_COMMON_YES           = "Yes";
const String PIX_HISTORY_XML_COMMON_NO            = "No";

const String PIX_HISTORY_XMLATTR_MODE             = "mode";
const String PIX_HISTORY_XMLATTR_PATH             = "path";
const String PIX_HISTORY_XMLATTR_NAME             = "name";
const String PIX_HISTORY_XMLATTR_TYPE             = "type";
const String PIX_HISTORY_XMLATTR_VAL              = "val";

enum PixReadMode {
    READ_MODE_UNKNOWN,
    READ_MODE_SCAN,
    READ_MODE_HIST,
};

enum PixScanType {
    SCAN_UNKNOWN,
    SCAN_THRESHOLD,
    SCAN_ENC,
    SCAN_SCURVE_CH2,
    SCAN_TOT,
    SCAN_T0,
};

enum PixValType {
    VAL_UNKNOWN,
    VAL_MEAN,
    VAL_RMS,
};

enum PixDisplayType {
    DISP_NONE         = 0,
    DISP_PIX_ALL      = 1,
    DISP_PIX_LAYER_0  = 2,
    DISP_PIX_LAYER_1  = 4,
    DISP_PIX_LAYER_2  = 8,
    DISP_PIX_EC       = 16,
    DISP_PIX_EC_ASIDE = 32,
    DISP_PIX_EC_CSIDE = 64,
    DISP_IBL_ALL      = 128,
    DISP_IBL_PLANAR   = 256,
    DISP_IBL_3D       = 512,
    DISP_IBL_3D_FBK   = 1024,
    DISP_IBL_3D_CNM   = 2048,
    DISP_MAX          = 4096,
};

enum PixHorizontalAxis {
    XAXIS_UNKNOWN,
    XAXIS_SCANFILENAME,
    XAXIS_DATE,
    XAXIS_LUMINOSITY,
    XAXIS_TID,
    XAXIS_CUSTOM,
};

struct PixConfigScanFile
{
    // Constructor
    PixConfigScanFile( const String& path = "", const String& name = "" )
        : m_path  ( path )
        , m_name  ( name ) { }

    String m_path; // calibration scan file
    String m_name; // label for x axis
};

struct PixConfigScanType
{
    PixScanType m_type; // calibration scan type
    PixValType  m_val;  // calibration scan value (mean or rms)
};

/////////////////////////////////////////////////////////////////////////
//
// CLASS : PixHistoryPlotParameter
//
/////////////////////////////////////////////////////////////////////////
class PixHistoryPlotParameter
{
public:
    PixHistoryPlotParameter( )
        : m_dispType        ( DISP_NONE )
        , m_showAllPixType  ( false )
        , m_xAxis           ( XAXIS_UNKNOWN )
        , m_fileName        ( "" ){ }

    ~PixHistoryPlotParameter( ) { }

    // Console output internal value (mainly for debug)
    void                           Print                 ( );
        
    bool     LoadConfigParam        ( PixHistoryPlotConfig*    pPlotCfg );

    PixReadMode                    m_readMode;
    std::list< PixConfigScanFile > m_scanFileList;
    PixConfigScanType              m_scanType;
    int                            m_dispType;
    bool                           m_showAllPixType;
    PixHorizontalAxis              m_xAxis;
    String                         m_fileName;

private:
    bool     LoadReadMode           ( PixHistoryPlotConfig*    pPlotCfg );
    bool     LoadScanFiles          ( PixHistoryPlotConfig*    pPlotCfg );
    bool     LoadScanType           ( PixHistoryPlotConfig*    pPlotCfg );
    bool     LoadDisplays           ( PixHistoryPlotConfig*    pPlotCfg );
    bool     LoadShowAllPixelType   ( PixHistoryPlotConfig*    pPlotCfg );
    bool     LoadHorizontalAxis     ( PixHistoryPlotConfig*    pPlotCfg );
    bool     LoadFileSaveOption     ( PixHistoryPlotConfig*    pPlotCfg );

};

#endif // PIX_HISTORY_PLOT_PARAM_H
